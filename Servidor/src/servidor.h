#ifndef __SERVIDOR_H__
#define __SERVIDOR_H__
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "mensajes.h"

#define LONGITUD_NOMBRE 255

//Posibles errores
#define EEXISTS 1
#define ENOOBJ 2
#define EFULL 3
#define EOFFSET 4
#define EIO 5
#define EPARAM 6

//Manejador de mensajes para el servidor
void manejaMensajesServidor(mensaje* msj, int fd);

#endif
