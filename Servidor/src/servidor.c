#include "servidor.h"

void manejaCreateObject(createObject* msj, int fd){

	printf("Recibida petición de creación de objeto\n");

	uint8_t longitud=msj->longitud;
	if(longitud ==0){

		fprintf(stderr,"Longitud de nombre de objeto inválida\n");
		mensaje* err= (mensaje*) creaError(EPARAM);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}
	char nombre[LONGITUD_NOMBRE];
	memset(&nombre, 0, LONGITUD_NOMBRE);
	for(int i=0;i<longitud;i++)
		nombre[i]=msj->nombre[i];

	if(access(nombre, F_OK) != -1){

		fprintf(stderr,"El objeto ya existe\n");
		mensaje* err= (mensaje*) creaError(EEXISTS);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}

	int filed;
	if((filed=open(nombre, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR)) < 0){

		int errnoCopia= errno;
		fprintf(stderr,"El objeto no se ha podido crear\n");
		mensaje* err;
		if(errnoCopia == ENOMEM) err= (mensaje*) creaError(EFULL);
		else err= (mensaje*) creaError(EIO);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}
	close(filed);
	ack* msjAck= creaAck();
	enviaMensaje(msjAck,fd);
	liberaMensaje(msjAck);
	printf("Objeto creado correctamente\n");
}

void manejaDeleteObject(deleteObject* msj, int fd){

	printf("Recibida petición de supresión de objeto\n");

	uint8_t longitud=msj->longitud;
	if(longitud ==0){

		fprintf(stderr,"Longitud de nombre de objeto inválida\n");
		mensaje* err= (mensaje*) creaError(EPARAM);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}
	char nombre[LONGITUD_NOMBRE];
	memset(&nombre, 0, LONGITUD_NOMBRE);
	for(int i=0;i<longitud;i++)
		nombre[i]=msj->nombre[i];

	if(access(nombre, F_OK) == -1){

		fprintf(stderr,"El objeto no existe\n");
		mensaje* err= (mensaje*) creaError(ENOOBJ);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}

	if(remove(nombre) < 0){

		fprintf(stderr,"El objeto no se ha podido eliminar\n");
		mensaje* err= (mensaje*) creaError(EIO);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}

	ack* msjAck= creaAck();
	enviaMensaje(msjAck,fd);
	liberaMensaje(msjAck);
	printf("Objeto eliminado correctamente\n");
}

void manejaReadObject(readObject* msj, int fd){

	printf("Recibida petición de lectura de objeto\n");

	uint8_t longitud=msj->longitud;
	if(longitud ==0){

		fprintf(stderr,"Longitud de nombre de objeto inválida\n");
		mensaje* err= (mensaje*) creaError(EPARAM);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}
	char nombre[LONGITUD_NOMBRE];
	memset(&nombre, 0, LONGITUD_NOMBRE);
	for(int i=0;i<longitud;i++)
		nombre[i]=msj->nombre[i];

	if(access(nombre, F_OK) == -1){

		fprintf(stderr,"El objeto no existe\n");
		mensaje* err= (mensaje*) creaError(ENOOBJ);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}

	int filed;
	if((filed=open(nombre, O_RDONLY)) < 0){

		fprintf(stderr,"El objeto no se ha podido abrir\n");
		mensaje* err= (mensaje*) creaError(EIO);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}

	uint64_t posicion=msj->posicion;
	uint64_t posicionReal=lseek(filed, posicion, SEEK_SET);

	if(posicionReal < 0){

		fprintf(stderr,"No se ha podido alcanzar la posición solicitada del objeto\n");
		mensaje* err= (mensaje*) creaError(EIO);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		close(filed);
		return;
	}

	if(posicionReal < posicion){

		fprintf(stderr,"No se ha podido alcanzar la posición solicitada del objeto\n");
		mensaje* err= (mensaje*) creaError(EOFFSET);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		close(filed);
		return;
	}
	uint64_t lecturaTotal = msj->espacio, leidoTotal = 0;
	int64_t leido = 0;

	char* buffer = (char*)malloc(sizeof(char)*lecturaTotal);

	while(leidoTotal < lecturaTotal && (leido= read(filed,buffer+leidoTotal,lecturaTotal-leidoTotal)) > 0) leidoTotal+=leido;

	if(leido < 0){

		fprintf(stderr,"El objeto no se ha podido leer\n");
		mensaje* err= (mensaje*) creaError(EIO);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		free(buffer);
		close(filed);
		return;
	}

	if(leidoTotal < lecturaTotal) printf("Peligro, esperaba leer %lld bytes más\n",lecturaTotal-leidoTotal);

	close(filed);
	mensaje* msjReadAck= (mensaje*) creaReadAck(leidoTotal, buffer);
	enviaMensaje(msjReadAck,fd);
	liberaMensaje(msjReadAck);
	printf("Objeto leído correctamente\n");
}

void manejaWriteObject(writeObject* msj, int fd){

	printf("Recibida petición de escritura de objeto\n");

	uint8_t longitud=msj->longitud;
	if(longitud ==0){

		fprintf(stderr,"Longitud de nombre de objeto inválida\n");
		mensaje* err= (mensaje*) creaError(EPARAM);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}
	char nombre[LONGITUD_NOMBRE];
	memset(&nombre, 0, LONGITUD_NOMBRE);
	for(int i=0;i<longitud;i++)
		nombre[i]=msj->nombre[i];

	if(access(nombre, F_OK) == -1){

		fprintf(stderr,"El objeto no existe\n");
		mensaje* err= (mensaje*) creaError(ENOOBJ);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}

	int filed;
	if((filed=open(nombre, O_WRONLY)) < 0){

		fprintf(stderr,"El objeto no se ha podido abrir\n");
		mensaje* err= (mensaje*) creaError(EIO);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}

	uint64_t posicion=msj->posicion;
	uint64_t posicionReal=lseek(filed, posicion, SEEK_SET);

	if(posicionReal < 0){

		fprintf(stderr,"No se ha podido alcanzar la posición solicitada del objeto\n");
		mensaje* err= (mensaje*) creaError(EIO);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		close(filed);
		return;
	}

	if(posicionReal < posicion){

		fprintf(stderr,"No se ha podido alcanzar la posición solicitada del objeto\n");
		mensaje* err= (mensaje*) creaError(EOFFSET);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		close(filed);
		return;
	}

	int64_t escrito= write(filed,msj->datos,msj->espacio);
	int errnoCopia = errno;

	if(escrito < 0){

		fprintf(stderr,"El objeto no se ha podido escribir\n");
		mensaje* err= (mensaje*) creaError(EIO);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		close(filed);
		return;
	}

	close(filed);
	mensaje* msjWriteAck= (mensaje*) creaWriteAck(escrito);
	enviaMensaje(msjWriteAck,fd);
	liberaMensaje(msjWriteAck);

	if(escrito < msj->espacio){

		printf("Peligro, esperaba leer %lld bytes más\n",msj->espacio-escrito);
		mensaje* err;
		if(errnoCopia == ENOMEM) err= (mensaje*) creaError(EFULL);
		else err= (mensaje*) creaError(EIO);
		enviaMensaje(err, fd);
		liberaMensaje(err);
	}
	else printf("Objeto escrito correctamente\n");
}

void manejaObjectSize(objectSize* msj, int fd){

	printf("Recibida petición de tamaño de objeto\n");

	uint8_t longitud=msj->longitud;
	if(longitud ==0){

		fprintf(stderr,"Longitud de nombre de objeto inválida\n");
		mensaje* err= (mensaje*) creaError(EPARAM);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}
	char nombre[LONGITUD_NOMBRE];
	memset(&nombre, 0, LONGITUD_NOMBRE);
	for(int i=0;i<longitud;i++)
		nombre[i]=msj->nombre[i];

	if(access(nombre, F_OK) == -1){

		fprintf(stderr,"El objeto no existe\n");
		mensaje* err= (mensaje*) creaError(ENOOBJ);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}

	struct stat st;
	if(stat(nombre, &st)<0){

		fprintf(stderr,"No se ha podido determinar el tamaño del objeto\n");
		mensaje* err= (mensaje*) creaError(EIO);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}

	printf("Tamaño enviado correctamente\n");
	mensaje* msjSizeAck= (mensaje*) creaSizeAck(st.st_size);
	enviaMensaje(msjSizeAck,fd);
	liberaMensaje(msjSizeAck);
}

void manejaTruncateObject(truncateObject* msj, int fd){

	printf("Recibida petición de truncado de objeto\n");

	uint8_t longitud=msj->longitud;
	if(longitud ==0){

		fprintf(stderr,"Longitud de nombre de objeto inválida\n");
		mensaje* err= (mensaje*) creaError(EPARAM);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}
	char nombre[LONGITUD_NOMBRE];
	memset(&nombre, 0, LONGITUD_NOMBRE);
	for(int i=0;i<longitud;i++)
		nombre[i]=msj->nombre[i];

	if(access(nombre, F_OK) == -1){

		fprintf(stderr,"El objeto no existe\n");
		mensaje* err= (mensaje*) creaError(ENOOBJ);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}

	if(truncate(nombre, msj->espacio)<0){

		fprintf(stderr,"El objeto no se puede truncar\n");
		mensaje* err= (mensaje*) creaError(EIO);
		enviaMensaje(err, fd);
		liberaMensaje(err);
		return;
	}
	
	ack* msjAck= creaAck();
	enviaMensaje(msjAck,fd);
	liberaMensaje(msjAck);
	printf("Objeto truncado correctamente\n");
}

void manejaMensajesServidor(mensaje* msj, int fd){

	switch(msj->tipo){
		case 3:
			manejaCreateObject((createObject*) msj, fd);
			break;
		case 4:
			manejaDeleteObject((deleteObject*) msj, fd);
			break;
		case 5:
			manejaReadObject((readObject*) msj, fd);
			break;
		case 7:
			manejaWriteObject((writeObject*) msj, fd);
			break;
		case 9:
			manejaObjectSize((objectSize*) msj, fd);
			break;
		case 11:
			manejaTruncateObject((truncateObject*) msj, fd);
			break;
		default:
			printf("Peligro, el servidor nunca debería recibir un mensaje de este tipo (%d)\n", msj->tipo);
			mensaje* err= (mensaje*) creaError(EPARAM);
			enviaMensaje(err, fd);
			liberaMensaje(err);
	}
printf("\tMuero 1\n");
	liberaMensaje(msj);
printf("\tMuero 2\n");
}
