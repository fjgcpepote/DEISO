#ifndef __LECTOESCRITURA_H__
#define __LECTOESCRITURA_H__
#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

//Lee n bytes
char* leenBytes(int n, int fd);

//Lee un byte y lo devuelve como uint8_t
uint8_t leeByte(int fd);

//Lee 8 bytes y los devuelve como uint64_t
uint64_t lee8Bytes(int fd);

//Escribe n bytes
void escribenBytes(const char* buffer, int n, int fd);

//Escribe un uint8_t
void escribeByte(uint8_t buffer, int fd);

//Escribe un uint64_t
void escribe8Bytes(uint64_t buffer, int fd);

#endif
