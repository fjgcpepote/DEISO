#include "mensajes.h"

void memoriaInsuficiente(){

	perror("Fallo al reservar memoria para crear mensaje\n");
	exit(6);
}

ack* creaAck(){

	ack* aux=(ack*)malloc(sizeof(ack));
	if(aux==NULL) memoriaInsuficiente();
	aux->tipo=1;
	return aux;
}

error* creaError(uint8_t codigo){

	error* aux=(error*)malloc(sizeof(error));
	if(aux==NULL) memoriaInsuficiente();
	aux->tipo=2;
	aux->codigo=codigo;
	return aux;
}

createObject* creaCreateObject(uint8_t longitud, const char* nombre){

	createObject* aux=(createObject*)malloc(sizeof(createObject));
	if(aux==NULL) memoriaInsuficiente();
	aux->tipo=3;
	aux->longitud=longitud;
	aux->nombre= nombre;
	return aux;
}

deleteObject* creaDeleteObject(uint8_t longitud, const char* nombre){

	deleteObject* aux=(deleteObject*)creaCreateObject(longitud, nombre);
	aux->tipo=4;
	return aux;
}

readObject* creaReadObject(uint8_t longitud, const char* nombre, uint64_t posicion, uint64_t espacio){

	readObject* aux=(readObject*)malloc(sizeof(readObject));
	if(aux==NULL) memoriaInsuficiente();
	aux->tipo=5;
	aux->longitud=longitud;
	aux->nombre = nombre;
	aux->posicion=posicion;
	aux->espacio=espacio;
	return aux;
}

readAck* creaReadAck(uint64_t espacio, const char* datos){

	readAck* aux=(readAck*)malloc(sizeof(readAck));
	if(aux==NULL) memoriaInsuficiente();
	aux->tipo=6;
	aux->espacio=espacio;
	aux->datos= datos;
	return aux;
}

writeObject* creaWriteObject(uint8_t longitud, const char* nombre, uint64_t posicion, uint64_t espacio, const char* datos){

	writeObject* aux=(writeObject*)malloc(sizeof(writeObject));
	if(aux==NULL) memoriaInsuficiente();
	aux->tipo=7;
	aux->longitud=longitud;
	aux->nombre=nombre;
	aux->posicion=posicion;
	aux->espacio=espacio;
	aux->datos= datos;
	return aux;
}

writeAck* creaWriteAck(uint64_t espacio){

	writeAck* aux=(writeAck*)malloc(sizeof(writeAck));
	if(aux==NULL) memoriaInsuficiente();
	aux->tipo=8;
	aux->espacio=espacio;
	return aux;
}

objectSize* creaObjectSize(uint8_t longitud, const char* nombre){

	objectSize* aux=(objectSize*)creaCreateObject(longitud, nombre);
	aux->tipo=9;
	return aux;
}

sizeAck* creaSizeAck(uint64_t espacio){

	sizeAck* aux=(sizeAck*)creaWriteAck(espacio);
	aux->tipo=10;
	return aux;
}

truncateObject* creaTruncateObject(uint8_t longitud, const char* nombre, uint64_t espacio){

	truncateObject* aux=(truncateObject*)malloc(sizeof(truncateObject));
	if(aux==NULL) memoriaInsuficiente();
	aux->tipo=11;
	aux->longitud=longitud;
	aux->nombre=nombre;
	aux->espacio=espacio;
	return aux;
}

mensaje* recibeAck(){

	mensaje* msj=creaAck();
	return msj;
}

mensaje* recibeError(int fd){

	uint8_t codigo=leeByte(fd);
	mensaje* msj=(mensaje*)creaError(codigo);
	return msj;
}

mensaje* recibeCreateObject(int fd){

	uint8_t longitud=leeByte(fd);
	const char* nombre=leenBytes(longitud,fd);
	mensaje* msj=(mensaje*)creaCreateObject(longitud,nombre);
	return msj;
}

mensaje* recibeDeleteObject(int fd){

	uint8_t longitud=leeByte(fd);
	const char* nombre=leenBytes(longitud,fd);
	mensaje* msj=(mensaje*)creaDeleteObject(longitud,nombre);
	return msj;
}

mensaje* recibeReadObject(int fd){

	uint8_t longitud=leeByte(fd);
	const char* nombre=leenBytes(longitud,fd);
	uint64_t posicion=lee8Bytes(fd);
	uint64_t espacio=lee8Bytes(fd);
	mensaje* msj=(mensaje*)creaReadObject(longitud,nombre,posicion,espacio);
	return msj;
}

mensaje* recibeReadAck(int fd){

	uint64_t espacio=lee8Bytes(fd);
	const char* datos=leenBytes(espacio,fd);
	mensaje* msj=(mensaje*)creaReadAck(espacio,datos);
	return msj;
}

mensaje* recibeWriteObject(int fd){

	uint8_t longitud=leeByte(fd);
	const char* nombre=leenBytes(longitud,fd);
	uint64_t posicion=lee8Bytes(fd);
	uint64_t espacio=lee8Bytes(fd);
	const char* datos=leenBytes(espacio,fd);
	mensaje* msj=(mensaje*)creaWriteObject(longitud,nombre,posicion,espacio,datos);
	return msj;
}

mensaje* recibeWriteAck(int fd){

	uint64_t espacio=lee8Bytes(fd);
	mensaje* msj=(mensaje*)creaWriteAck(espacio);
	return msj;
}

mensaje* recibeObjectSize(int fd){

	uint8_t longitud=leeByte(fd);
	const char* nombre=leenBytes(longitud,fd);
	mensaje* msj=(mensaje*)creaObjectSize(longitud,nombre);
	return msj;
}

mensaje* recibeSizeAck(int fd){

	uint64_t espacio=lee8Bytes(fd);
	mensaje* msj=(mensaje*)creaSizeAck(espacio);
	return msj;
}

mensaje* recibeTruncateObject(int fd){

	uint8_t longitud=leeByte(fd);
	const char* nombre=leenBytes(longitud,fd);
	uint64_t espacio=lee8Bytes(fd);
	mensaje* msj=(mensaje*)creaTruncateObject(longitud,nombre,espacio);
	return msj;
}

void enviaAck(mensaje* msj, int fd){

	escribeByte(msj->tipo,fd);
}

void enviaError(mensaje* msj, int fd){

	error* msjError=(error*)msj;
	escribeByte(msjError->tipo,fd);
	escribeByte(msjError->codigo,fd);
}

void enviaCreateObject(mensaje* msj, int fd){

	createObject* msjCreateObject=(createObject*)msj;
	escribeByte(msjCreateObject->tipo,fd);
	escribeByte(msjCreateObject->longitud,fd);
	escribenBytes(msjCreateObject->nombre,msjCreateObject->longitud,fd);
}

void enviaDeleteObject(mensaje* msj, int fd){

	enviaCreateObject(msj,fd);
}

void enviaReadObject(mensaje* msj, int fd){

	readObject* msjReadObject=(readObject*)msj;
	escribeByte(msjReadObject->tipo,fd);
	escribeByte(msjReadObject->longitud,fd);
	escribenBytes(msjReadObject->nombre,msjReadObject->longitud,fd);
	escribe8Bytes(msjReadObject->posicion,fd);
	escribe8Bytes(msjReadObject->espacio,fd);
}

void enviaReadAck(mensaje* msj, int fd){

	readAck* msjReadAck=(readAck*)msj;
	escribeByte(msjReadAck->tipo,fd);
	escribe8Bytes(msjReadAck->espacio,fd);
	escribenBytes(msjReadAck->datos,msjReadAck->espacio,fd);
}

void enviaWriteObject(mensaje* msj, int fd){

	writeObject* msjWriteObject=(writeObject*)msj;
	escribeByte(msjWriteObject->tipo,fd);
	escribeByte(msjWriteObject->longitud,fd);
	escribenBytes(msjWriteObject->nombre,msjWriteObject->longitud,fd);
	escribe8Bytes(msjWriteObject->posicion,fd);
	escribe8Bytes(msjWriteObject->espacio,fd);
	escribenBytes(msjWriteObject->datos,msjWriteObject->espacio,fd);
}

void enviaWriteAck(mensaje* msj, int fd){

	writeAck* msjWriteAck=(writeAck*)msj;
	escribeByte(msjWriteAck->tipo,fd);
	escribe8Bytes(msjWriteAck->espacio,fd);
}

void enviaObjectSize(mensaje* msj, int fd){

	enviaCreateObject(msj,fd);
}

void enviaSizeAck(mensaje* msj, int fd){

	enviaWriteAck(msj,fd);
}

void enviaTruncateObject(mensaje* msj, int fd){

	truncateObject* msjTruncateObject=(truncateObject*)msj;
	escribeByte(msjTruncateObject->tipo,fd);
	escribeByte(msjTruncateObject->longitud,fd);
	escribenBytes(msjTruncateObject->nombre,msjTruncateObject->longitud,fd);
	escribe8Bytes(msjTruncateObject->espacio,fd);
}

void liberaMensaje(mensaje* msj){

	//Leo el primer byte para saber el tipo
	switch(msj->tipo){

		case 1:
		case 2:
		case 8:
		case 10: break;
		case 3:
		case 4:
		case 9: ;
			createObject* cO=(createObject*) msj;
			free((char*) cO->nombre);
			break;
		case 5: ;
			readObject* rO=(readObject*) msj;
			free((char*) rO->nombre);
			break;
		case 6: ;
			readAck* rA=(readAck*) msj;
			free((char*) rA->datos);
			break;
		case 7: ;
			writeObject* wO=(writeObject*) msj;
			free((char*) wO->nombre);
			free((char*) wO->datos);
			break;
		case 11: ;
			truncateObject* tO=(truncateObject*) msj;
			free((char*) tO->nombre);
			break;
		default: fprintf(stderr, "Fallo al liberar un mensaje, tipo desconocido %d\n",msj->tipo);
			exit(8);
	}
	free(msj);
}

mensaje* recibeMensaje(int fd){

	//Leo el primer byte para saber el tipo
	uint8_t tipo=leeByte(fd);
	if (tipo==0) return NULL;
	switch(tipo){

		case 1: return recibeAck(fd);
		case 2: return recibeError(fd);
		case 3: return recibeCreateObject(fd);
		case 4: return recibeDeleteObject(fd);
		case 5: return recibeReadObject(fd);
		case 6: return recibeReadAck(fd);
		case 7: return recibeWriteObject(fd);
		case 8: return recibeWriteAck(fd);
		case 9: return recibeObjectSize(fd);
		case 10: return recibeSizeAck(fd);
		case 11: return recibeTruncateObject(fd);
		default: fprintf(stderr, "Fallo al recibir un mensaje, tipo desconocido %d\n",tipo);
			exit(8);
	}
}

void enviaMensaje(mensaje* msj, int fd){

	switch(msj->tipo){

		case 1: enviaAck(msj,fd);
			return;
		case 2: enviaError(msj,fd);
			return;
		case 3: enviaCreateObject(msj,fd);
			return;
		case 4: enviaDeleteObject(msj,fd);
			return;
		case 5: enviaReadObject(msj,fd);
			return;
		case 6: enviaReadAck(msj,fd);
			return;
		case 7: enviaWriteObject(msj,fd);
			return;
		case 8: enviaWriteAck(msj,fd);
			return;
		case 9: enviaObjectSize(msj,fd);
			return;
		case 10: enviaSizeAck(msj,fd);
			return;
		case 11:  enviaTruncateObject(msj,fd);
			return;
		default:
			fprintf(stderr, "Fallo al enviar un mensaje, tipo desconocido %d\n", msj->tipo);
			exit(8);
	}
}
