#ifndef __SOCKET_H__
#define __SOCKET_H__
#include <netinet/in.h>
#include <sys/wait.h>
#include "servidor.h"

//Crea un socket con el puerto especificado
int creaSocket(long numPuerto);

//Acepta una conexion y crea un hilo para manejar la comunicacion
void aceptaConexion(int sockfd);

//Cierra un socket
void cierraSocket(int sockfd);

#endif
