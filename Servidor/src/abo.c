#include <signal.h>
#include "socket.h"

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE
#endif

static int fin = 0;

void entradaErronea(){

	fprintf(stderr,"Uso: ./abo -d directorio -p puerto\n");
	exit(1);
}

void leeParametros(int argc, char *argv[], long* numPuerto){

	char* directorio=NULL;
	int control=0, opt;
	while ((opt = getopt(argc, argv, "d:p:")) != -1) {
		switch (opt) {
			case 'd':
				if(directorio!=NULL) entradaErronea();
				directorio = optarg;
				control|=1;
				break;
			case 'p': ;
				if(*numPuerto!=0) entradaErronea();
				char* comprobacion=NULL;
				*numPuerto = strtol(optarg,&comprobacion,10);

				if(!(optarg != NULL && optarg[0]!='\0' && comprobacion != NULL && comprobacion[0]=='\0')){
					fprintf(stderr,"El puerto indicado no es un número\n");
					exit(2);
				}
				control|=2;
				break;
			default: entradaErronea();
		}
	}

	if(control != 3) entradaErronea();

	if(*numPuerto<0 || *numPuerto>65535){

		fprintf(stderr,"El número de puerto ha de estar entre 0 y 65535\n");
		exit(2);
	}

	if(*numPuerto<1024) printf("Peligro, solo el súperusuario puede usar los puertos en el rango 0-1023\n");

	if(*numPuerto<49152) printf("Peligro, se aconseja usar un puerto en el rango 49152-65535\n");

	if(chdir(directorio)<0){

		fprintf(stderr,"El directorio indicado no es válido\n");
		exit(2);
	}
}

void manejador(int sig){

	if(sig==SIGINT) fin++;
	else {
		printf("Peligro, señal captada distinta de SIGINT\n");
	}
}

void compruebaSig(int output){

	if(output<0){

		perror("Fallo al intentar manejar SIGINT\n");
		exit(5);
	}
}

int main(int argc, char *argv[]){
	
	long numPuerto=0;
	leeParametros(argc, argv, &numPuerto);

	int sockfd=creaSocket(numPuerto);

	struct sigaction sa;
	memset(&sa, 0, sizeof(struct sigaction));
    sa.sa_handler = manejador;
	compruebaSig(sigemptyset(&sa.sa_mask));
    compruebaSig(sigaction(SIGINT, &sa, NULL));

	while(!fin) aceptaConexion(sockfd);

	printf("Detectada señal SIGINT, saliendo de la aplicación\n");

	cierraSocket(sockfd);

	return 0;
}
