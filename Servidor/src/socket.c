#include "socket.h"

int creaSocket(long numPuerto){

	//Abrir socket
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){

		perror("Fallo al abrir socket\n");
		exit(3);
	}

	//Inicializar socket
	struct sockaddr_in serv_addr;
	memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(numPuerto);

	//Asociamos ip y puerto al socket
	if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){

		perror("Fallo al hacer bind al socket\n");
		exit(3);
	}

	return sockfd;
}

void aceptaConexion(int sockfd){

	//Ponemos a escuchar al socket la peticion entrante
	if(listen(sockfd,1)<0){

		perror("Fallo al escuchar\n");
		exit(4);
	}

	//Aceptamos la conexion
	struct sockaddr_in cli_addr;
	unsigned int cli_size=sizeof(cli_addr);
	int newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &cli_size);

	if (newsockfd < 0) {
		//Signal captada mientras se esperaba
		if(errno == EINTR) return;
		perror("Fallo aceptando conexion entrante\n");
		exit(4);
	}

	//Creamos un proceso hijo para atender la comunicacion y poder seguir escuchando
	pid_t pid=fork();
	if(pid<0){

		perror("Fallo al crear un proceso\n");
		exit(9);
	}

	//Codigo del hijo
	if(pid==0){

		pid=fork();
		if(pid<0){

			perror("Fallo al crear un proceso\n");
			exit(9);
		}

		//Generamos un proceso nieto y matamos al hijo,
		//de esta manera el nieto no quedara en estado zombie al acabar
		if(pid==0){
			mensaje* msj;
			//El hijo acaba cuando el cliente no envie mas datos
			while((msj= recibeMensaje(newsockfd))!=NULL) manejaMensajesServidor(msj, newsockfd);

			printf("Conexión terminada, cerrando socket y saliendo del proceso\n");
			cierraSocket(newsockfd);
			exit(0);
		}
		else exit(0);
	}
	else{
		int status;
		if(waitpid(pid, &status, 0)<0){

			perror("Fallo al esperar a un proceso\n");
			exit(9);
		}
		if (!WIFEXITED(status)) fprintf(stderr,"El hijo con pid %d no ha acabado correctamente\n", pid);
	}
}

void cierraSocket(int sockfd){

	shutdown(sockfd,SHUT_RDWR);
	close(sockfd);
}
