#ifndef __SOCKET_H__
#define __SOCKET_H__
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include "mensajes.h"

//Se conecta a un socket con la IP y el puerto especificados
int creaSocketAux(char* IP, long numPuerto);

//Cierra el socket
void cierraSocketAux(int sockfd);

#endif
