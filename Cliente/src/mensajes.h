#ifndef __MENSAJES_H__
#define __MENSAJES_H__

#include "lectoescritura.h"

//Tipo general
typedef struct Mensaje{

	uint8_t tipo;
}mensaje;

//Tipo 1
typedef struct Mensaje ack;

//Tipo 2
typedef struct Error{

	uint8_t tipo;
	uint8_t codigo;
}error;

//Tipo 3
typedef struct CreateObject{

	uint8_t tipo;
	uint8_t longitud;
	const char* nombre;
}createObject;

//Tipo 4
typedef struct CreateObject deleteObject;

//Tipo 5
typedef struct ReadObject{

	uint8_t tipo;
	uint8_t longitud;
	const char* nombre;
	uint64_t posicion;
	uint64_t espacio;
}readObject;

//Tipo 6
typedef struct ReadAck{

	uint8_t tipo;
	uint64_t espacio;
	const char* datos;
}readAck;

//Tipo 7
typedef struct WriteObject{

	uint8_t tipo;
	uint8_t longitud;
	const char* nombre;
	uint64_t posicion;
	uint64_t espacio;
	const char* datos;
}writeObject;

//Tipo 8
typedef struct WriteAck{

	uint8_t tipo;
	uint64_t espacio;
}writeAck;

//Tipo 9
typedef struct CreateObject objectSize;

//Tipo 10
typedef struct WriteAck sizeAck;

//Tipo 11
typedef struct TruncateObject{

	uint8_t tipo;
	uint8_t longitud;
	const char* nombre;
	uint64_t espacio;
}truncateObject;

//Constructores para todos los mensajes
ack* creaAck();
error* creaError(uint8_t codigo);
createObject* creaCreateObject(uint8_t longitud, const char* nombre);
deleteObject* creaDeleteObject(uint8_t longitud, const char* nombre);
readObject* creaReadObject(uint8_t longitud, const char* nombre, uint64_t posicion, uint64_t espacio);
readAck* creaReadAck(uint64_t espacio, const char* datos);
writeObject* creaWriteObject(uint8_t longitud, const char* nombre, uint64_t posicion, uint64_t espacio, const char* datos);
writeAck* creaWriteAck(uint64_t espacio);
objectSize* creaObjectSize(uint8_t longitud, const char* nombre);
sizeAck* creaSizeAck(uint64_t espacio);
truncateObject* creaTruncateObject(uint8_t longitud, const char* nombre, uint64_t espacio);

//Libera la memoria de un mensaje
void liberaMensaje(mensaje* msj);

//Lee un mensaje de un socket
mensaje* recibeMensaje(int fd);

//Escribe un mensaje en un socket
void enviaMensaje(mensaje* msj, int fd);

#endif
