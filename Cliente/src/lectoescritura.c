﻿#include "lectoescritura.h"

char* leenBytes(int n, int fd){

	int leidoTotal = 0, leido = 0;
	char* buffer = (char*)malloc(sizeof(char)*n);
	if(buffer==NULL){

		log_msg("Fallo al reservar memoria para leer del socket\n%s\n", strerror(errno));
		exit(6);
	}
	memset(buffer, 0, n+1);

	while(leidoTotal < n && (leido= read(fd,buffer+leidoTotal,n-leidoTotal)) > 0) leidoTotal+=leido;

	if(n!=0 && leidoTotal == 0 && leido == 0) return NULL;

	if(leido < 0){

	log_msg("Fallo leyendo del socket, quizá la comunicación haya acabado\n%s\n", strerror(errno));
	exit(7);
	}

	if(leidoTotal != n) log_msg("Peligro, esperaba leer %d bytes más\n",n-leidoTotal);

	return buffer;
}

uint8_t leeByte(int fd){

	uint8_t* ptr = (uint8_t*)leenBytes(1,fd);
	uint8_t leido;
	if(ptr == NULL) leido=0;
	else leido = *ptr;
	free(ptr);
	return leido;
}

uint64_t lee8Bytes(int fd){

	uint64_t* ptr = (uint64_t*)leenBytes(8,fd);
	uint64_t leido = *ptr;
	free(ptr);
	return leido;
}

void escribenBytes(const char* buffer, int n, int fd){

	int escrito;
	escrito= write(fd,buffer,n);
	if(escrito < 0){

		log_msg("Fallo escribiendo en el socket, quizá la comunicación haya acabado\n%s\n", strerror(errno));
		exit(7);
	}
	if(escrito != n) log_msg("Peligro, esperaba escribir %d bytes más\n",n-escrito);
}

void escribeByte(uint8_t buffer, int fd){

	escribenBytes((const char*) &buffer, 1, fd);
}

void escribe8Bytes(uint64_t buffer, int fd){

	escribenBytes((const char*) &buffer, 8, fd);
}
