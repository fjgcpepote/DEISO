#include "socket.h"

int creaSocketAux(char* IP, long numPuerto){

	//Abrir socket
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){

		log_msg("Fallo al abrir socket\n%s\n", strerror(errno));
		exit(3);
	}

	//Convertimos el puerto (el numero mas grande tendra 5 cifras)
	char charPuerto[5];
	if(sprintf(charPuerto, "%ld", numPuerto)<0){

		log_msg("Fallo al intentar convertir el puerto a char*\n");
		exit(3);
	}

	//Obtenemos todas las direcciones posibles
	struct addrinfo* direcciones;
	int error;
	if((error=getaddrinfo(IP, charPuerto, NULL, &direcciones))!=0){

		log_msg("Fallo al intentar obtener la dirección del servidor: %s\n", gai_strerror(error));
		exit(3);
	}

	//Intentamos conectarnos a cualquier direccion
	for(struct addrinfo* direccion=direcciones; direccion!=NULL; direccion=direccion->ai_next){

		sockfd=socket(direccion->ai_family, direccion->ai_socktype, direccion->ai_protocol);
        if(sockfd<0) log_msg("Fallo al abrir socket\n%s\n", strerror(errno));
        else{

			if(connect(sockfd, direccion->ai_addr, direccion->ai_addrlen)<0){

				log_msg("Fallo al conectar al servidor\n%s\n", strerror(errno));
				close(sockfd);
			}
			else {
				freeaddrinfo(direcciones);
				return sockfd;
			}
		}
	}

	log_msg("Fallo al intentar conectarse a todas las direcciones conocidas del servidor\n");
	freeaddrinfo(direcciones);
	exit(3);
}

void cierraSocketAux(int sockfd){

	shutdown(sockfd,SHUT_RDWR);
	close(sockfd);
}
