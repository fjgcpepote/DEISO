#ifndef __CLIENTE_H__
#define __CLIENTE_H__
#include <string.h>
#include "socket.h"

#define LONGITUD_NOMBRE 255
//Imprimible sin espacio
#define PRIMER_CHAR_PERMITIDO 33
//Imprimible sin DEL
#define ULTIMO_CHAR_PERMITIDO 126

#define CHAR_PROHIBIDOS 8
void creaSocket(char* IP, long numPuerto);

void cierraSocket();

//Crea un nombre aleatorio para los ficheros de metadatos
char* creaNombre(uint8_t* longitud);

//Escribe en un fichero de metadatos el nombre que almacenara el servidor
int creaMetaDatos(char* nombreFichero, char* nombre, uint8_t longitud);

//Recupera el nombre almacenado en el servidor a partir del nombre del archivo de metadatos
char* recuperaNombre(char* nombreFichero);

//Manejador de mensajes para el cliente
int manejaMensajesCliente(mensaje* msj, char* buffer, uint64_t* espacio);

#endif
