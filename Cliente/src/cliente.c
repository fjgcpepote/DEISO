#include "cliente.h"

static int sockfd;

// 34 " 42 * 92 \ 47 / 58 : 60 < 62 > 124 |
const static uint8_t charProhibido[CHAR_PROHIBIDOS] = { 34, 42, 47, 58, 60, 62, 92, 124};

void creaSocket(char* IP, long numPuerto){

	sockfd=creaSocketAux(IP, numPuerto);
}

void cierraSocket(){

	cierraSocketAux(sockfd);
}

int esCharPermitido(uint8_t caracter){

	for(int i=0; i<CHAR_PROHIBIDOS ; i++){

		if(caracter == charProhibido[i])
			return 0;
	}
	return 1;
}

char* creaNombre(uint8_t* longitud){

	//Numero aleatorio de caracteres
	*longitud=1+rand()%(LONGITUD_NOMBRE-1);
	char* nombre=(char*)malloc(*longitud*sizeof(char));
	uint8_t caracter;

	for(int i=0; i<*longitud; i++){

		//Genera un valor ascii aleatorio hasta que sea valido
		do caracter= PRIMER_CHAR_PERMITIDO + rand()%(ULTIMO_CHAR_PERMITIDO - PRIMER_CHAR_PERMITIDO);
		while(!esCharPermitido(caracter));
		nombre[i]=caracter;
	}

	return nombre;
}

int creaMetaDatos(char* nombreFichero, char* nombre, uint8_t longitud){

	FILE* fichero= fopen(nombreFichero, "w");
	if(fichero == NULL){

		log_msg("Fallo al abrir un fichero de metadatos\n%s", strerror(errno));
		return -1;
	}

	if(fwrite(nombre, sizeof(char), longitud, fichero)!=longitud){

		log_msg("Fallo al escribir en un fichero de metadatos\n");
		return -1;
	}

	if(fclose(fichero) == EOF){

		log_msg("Fallo al cerrar un fichero de metadatos\n%s\n", strerror(errno));
		return -1;
	}
	return 0;
}

char* recuperaNombre(char* nombreFichero){

	char* nombre= (char*)malloc(sizeof(char)*(LONGITUD_NOMBRE));
	memset(nombre, 0, LONGITUD_NOMBRE);
	
	FILE* fichero= fopen(nombreFichero, "r");
	if(fichero == NULL){

		log_msg("Fallo al abrir un fichero de metadatos\n%s\n",strerror(errno));
		return NULL;
	}

	if(fread(nombre, sizeof(char), LONGITUD_NOMBRE, fichero)==0 && ferror(fichero)){

		log_msg("Fallo al leer un fichero de metadatos\n");
		return NULL;
	}

	if(fclose(fichero) == EOF){

		log_msg("Fallo al cerrar un fichero de metadatos\n%s\n", strerror(errno));
		return NULL;
	}

	return nombre;
}

int compruebaAck(mensaje* msj){

	int resultado;
	switch(msj->tipo){
		case 1:
			resultado=0;
			break;
		case 2: ;
			error* msjError=(error*) msj;
			resultado=msjError->codigo;
			break;
		default:
			log_msg("Peligro, el cliente no debería haber recibido esta respuesta\n");
			//Al tener parametros incorrectos, devolvemos EPARAM
			//No podemos liberar el mensaje porque no sabemos su tipo
			return 6;
	}
	liberaMensaje(msj);
	return resultado;
}

int compruebaRead(mensaje* msj, char* buffer){

	int resultado;
	switch(msj->tipo){
		case 6: ;
			readAck* msjRead= (readAck*) msj;
			resultado=msjRead->espacio;
			//Se podria comparar resultado y el contenido del mensaje tras esto
			//para ver si se ha perdido informacion
			memcpy(buffer, msjRead->datos, resultado);
			break;
		case 2: ;
			error* msjError=(error*) msj;
			resultado=-msjError->codigo;
			break;
		default:
			log_msg("Peligro, el cliente no debería haber recibido esta respuesta\n");
			//Al tener parametros incorrectos, devolvemos EPARAM
			//No podemos liberar el mensaje porque no sabemos su tipo
			return -6;
	}
	liberaMensaje(msj);
	return resultado;
}

int compruebaWrite(mensaje* msj, uint64_t* espacio){

	int resultado;
	switch(msj->tipo){
		case 8: ;
			writeAck* msjWrite= (writeAck*) msj;
			resultado=msjWrite->espacio;
			//Se podria comparar resultado y el contenido del mensaje tras esto
			//para ver si se ha perdido informacion
			//Si no se ha escrito cuanto se esperaba, deberia llegar un segundo mensaje de tipo error
			if(msjWrite->espacio!=*espacio){

				liberaMensaje(msj);
				msj=recibeMensaje(sockfd);
				*espacio=compruebaAck(msj);
				return resultado;
			}
			break;
		case 2: ;
			error* msjError=(error*) msj;
			resultado=-msjError->codigo;
			break;
		default:
			log_msg("Peligro, el cliente no debería haber recibido esta respuesta\n");
			//Al tener parametros incorrectos, devolvemos EPARAM
			//No podemos liberar el mensaje porque no sabemos su tipo
			return -6;
	}
	liberaMensaje(msj);
	return resultado;
}

int compruebaSize(mensaje* msj){

	int resultado;
	switch(msj->tipo){
		case 10: ;
			sizeAck* msjWrite= (sizeAck*) msj;
			resultado=msjWrite->espacio;
			break;
		case 2: ;
			error* msjError=(error*) msj;
			resultado=-msjError->codigo;
			break;
		default:
			log_msg("Peligro, el cliente no debería haber recibido esta respuesta\n");
			//Al tener parametros incorrectos, devolvemos EPARAM
			//No podemos liberar el mensaje porque no sabemos su tipo
			return -6;
	}
	liberaMensaje(msj);
	return resultado;
}

int manejaMensajesCliente(mensaje* msj, char* buffer, uint64_t* espacio){

	int resultado;
	int tipo=msj->tipo;

	enviaMensaje(msj, sockfd);
	liberaMensaje(msj);
	msj=recibeMensaje(sockfd);

	switch(tipo){
		case 3:
		case 4:
		case 11:
			resultado=compruebaAck(msj);
			break;
		case 5:
			resultado=compruebaRead(msj, buffer);
			break;
		case 7:
			resultado=compruebaWrite(msj, espacio);
			break;
		case 9:
			resultado=compruebaSize(msj);
			break;
		default:
			log_msg("Peligro, el cliente nunca debería enviar un mensaje de este tipo\n");
			//Al tener parametros incorrectos, devolvemos EPARAM
			resultado=6;
	}
	return resultado;
}
